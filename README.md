# Meetup Abril 2020

## Palestras

[Abertura](https://devopspbs.gitlab.io/meetups/meetup-2020-04-09/)

Analise de dados com Jupyther Notebook - Dhony Silva

Construindo dashboard's com Vuejs e Django Rest Framework - Thiago Almeida

> As apresentações estão disponíveis no diretório `presentations`.

## License

GPLv3 or later.
