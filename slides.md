<!-- .slide: data-background-image="images/online-meetup.jpg" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----  ----

## O Que Temos Para Hoje

* Analise de dados com Jupyther Notebook - Dhony Silva
* Construindo dashboard's com Vuejs e Django Rest Framework - Thiago Almeida

----  ----

<a href="https://www.meetup.com/devopspbs/"><img width="240" src="images/DEVOLPBS.png" alt="DevOpsPBS"></a>

Grupo de Entusiastas, Estudantes e Profissionais de TI de Parauapebas (Desenvolvedores, Sysadmins, Infosecs, QAs, Engs/Admins de rede, Técnicos de informática, Analistas, Web Designers, etc)

----  ----

## Conduta

"Participe de maneira autêntica e ativa. Ao fazer isso, você contribui para a saúde e a longevidade dessa comunidade."

[//]: # (https://garoa.net.br/wiki/C%C3%B3digo_de_Conduta_Completo)

----  ----

## Site e Mídias Sociais

* **Site oficial:** [devopspbs.org](https://devopspbs.org)

* **Mídias sociais:**
  - [meetup.com/devopspbs](https://www.meetup.com/devopspbs/)
  - [gitlab.com/devopspbs](https://gitlab.com/devopspbs)
  - [instagram/devopspbs](https://www.instagram.com/devopspbs/)

* **Grupos de discussão:**
  - Grupo [DevOpsPBS](https://t.me/joinchat/A-G57xd_fjAnQwOzV_sdeQ) no Telegram
  - Grupo [TECNOLOGIA PBS](https://chat.whatsapp.com/7XfTiu53icSKKbANTQnMGH) no Whatsapp

----  ----

## Dica: Linkedin Mobile

![](images/in.png)
